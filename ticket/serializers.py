from rest_framework import serializers
from .models import Ticket,  Message
from django.contrib.auth import get_user_model

class TicketSerializer(serializers.ModelSerializer):
	class Meta:
		model = Ticket
		fields = '__all__'
		read_only_fields = ('date','admin', 'sender', 'status')



class UserSerializer(serializers.ModelSerializer):
	class Meta:
		model = get_user_model()
		fields = ('username',)

class FullUserSerializer(serializers.ModelSerializer):
	class Meta:
		model = get_user_model()
		fields = ('username','first_name', 'last_name', 'email')




class MessageSerializer(serializers.ModelSerializer):
	class Meta:
		model =  Message
		fields = '__all__'
		read_only_fields = ('sender','parent_id','id','ticket')
