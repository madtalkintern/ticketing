from django.urls import path
from . import views
urlpatterns = [
	path('tickets/', views.list_tickets, name='ticket_list'),
	path('ticket/', views.TicketCreat.as_view()),
	path('ticket/<int:pk>/', views.detail_ticket),
	path('ticket/<int:pk>/message/', views.MessageCreate.as_view()),
]