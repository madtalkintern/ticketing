from django.db import models
from django.contrib.auth import get_user_model


class Ticket(models.Model):
	class Status(models.IntegerChoices):
		OPEN = 0
		ANSWERING = 1
		CLOSE = 2
	sender = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
	subject = models.CharField(max_length=50)
	date = models.DateTimeField(auto_now=True)
	status = models.SmallIntegerField(choices=Status.choices, default=0)
	admin = models.ForeignKey(get_user_model(), on_delete=models.SET_NULL, default=None, null=True, blank=True, related_name="admin")
	


class Message(models.Model):
	sender = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
	ticket = models.ForeignKey(Ticket, on_delete=models.CASCADE)
	body = models.TextField()
	date = models.DateTimeField(auto_now_add=True)
	parent_id = models.ForeignKey('self', on_delete = models.CASCADE, null=True, blank=True)

