from django.shortcuts import get_object_or_404
from .serializers import TicketSerializer, UserSerializer, FullUserSerializer, MessageSerializer
from rest_framework import status, generics, decorators, response
from .models import Ticket, Message
from django.contrib.auth import get_user_model
from django.db.models import Q
# from django.core.mail import send_mail
from datetime import date

@decorators.api_view(['GET'])
def list_tickets(request):
	if request.user.is_superuser:
		qs = Ticket.objects.filter(Q(admin=request.user.id) | Q(admin__isnull=True)).filter(~Q(status=2)).distinct().order_by('id')
	else:
		qs = Ticket.objects.filter(sender=request.user.id)

	status = request.GET.get('status')

	from_date = request.GET.get('from_date')
	to_date = request.GET.get('to_date')

	if from_date is not None and to_date is not None:
		qs.filter(Q(date__gte=date(from_date))&Q(date__lte=date(to_date))).distinct()
	elif from_date is not None:
		qs.filter(date__gte=date(from_date))
	elif to_date is not None:
		qs.filter(date__lte=date(to_date))

	if status is not None:
		qs = qs.filter(status=status)

	tickets = TicketSerializer(qs,many = True).data
	result=[]
	for ticket in tickets:
		dic_ticket = dict(ticket)
		if dic_ticket['admin'] is not None:
			dic_ticket.update({"admin_obj" : UserSerializer(get_user_model().objects.get(id=dic_ticket['admin'])).data})
		if dic_ticket['sender'] is not None:
			dic_ticket.update({"sender_obj" : UserSerializer(get_user_model().objects.get(id=dic_ticket['sender'])).data})
		result.append(dic_ticket)

	return response.Response({"result":result}, status=status.HTTP_200_OK)



@decorators.api_view(['GET','PATCH'])
def detail_ticket(request, pk):
	if request.method == 'PATCH':
		ticket = get_object_or_404(Ticket, pk=pk)
		if request.user.is_superuser and request.data.get('status'):
			ticket.status = request.data.get('status')
			ticket.save()
			return response.Response({"result":"status changed"}, status=status.HTTP_200_OK)
		elif request.user == ticket.sender and request.data.get('subject'):
			ticket.subject = request.data.get('subject')
			ticket.save()
			return response.Response({"result":"subject changed"}, status=status.HTTP_200_OK)
		return response.Response({"error":"bad request"}, status=status.HTTP_400_BAD_REQUEST)
	else:
		ticket_qu = Ticket.objects.get(pk=pk)
		if not request.user.is_superuser and request.user != ticket_qu.user:
			return response.Response({"error":"You dont have promition to accse the ticket"}, status=403)
		ticket = TicketSerializer(ticket_qu).data
		if ticket is not None:
			dic_ticket = dict(ticket)
			if dic_ticket['admin'] is not None:
				dic_ticket.update({"admin_obj" : FullUserSerializer(get_user_model().objects.get(id=dic_ticket['admin'])).data})
			if dic_ticket['sender'] is not None:
				dic_ticket.update({"sender_obj" : FullUserSerializer(get_user_model().objects.get(id=dic_ticket['sender'])).data})
		message_qu = Message.objects.filter(Q(ticket__id=pk)).distinct().order_by('parent_id')
		messages = []
		for message in MessageSerializer(message_qu, many = True).data:
			messages.append(message)
		dic_ticket.update({"messages" : messages})
		return response.Response({"result":dic_ticket}, status=status.HTTP_200_OK)



class MessageCreate(generics.CreateAPIView):
	serializer_class = MessageSerializer
	queryset = Message.objects.all()

	def post(self, request, *args, **kwargs):
		serializer = self.get_serializer(data=request.data)
		serializer.is_valid(raise_exception=True)
		pmid  = self.request.GET.get('pmid')
		ticket = get_object_or_404(Ticket,pk=self.kwargs['pk']).id
		if pmid is not None:
			if get_object_or_404(Message, pk=int(pmid)).ticket == get_object_or_404(Ticket,pk=self.kwargs['pk']):
				parent = Message.objects.get(pk=int(pmid))
				serializer.save(sender=self.request.user, parent_id=parent, ticket_id = ticket)
		else:
			serializer.save(sender=self.request.user, ticket_id = ticket)

		if Ticket.objects.get(pk=self.kwargs['pk']).admin is None:
			if self.request.user.is_superuser:
				ticket = Ticket.objects.filter(pk=self.kwargs['pk']).update(admin=self.request.user,status=1)
		headers = self.get_success_headers(serializer.data)
		# sub = "Ticket ["+str(ticket)+"]"
		# if request.user == get_object_or_404(Ticket, pk=ticket):
		# 	body = "You have new response on your ticket: \n\n\n"+self.request.data.get('body')
		# else:
		# 	body = "You added new message on your ticket: \n\n\n"+self.request.data.get('body')
		# dist = []
		# dist.append(get_object_or_404(Ticket, pk=ticket).sender.email)
		# send_gmail(sub, body, dist)
		return response.Response({"result":serializer.data}, status=status.HTTP_201_CREATED, headers=headers)


class TicketCreat(generics.CreateAPIView):
	serializer_class = TicketSerializer
	queryset = Ticket.objects.all()
	def perform_create(self, serializer):
		serializer.save(sender=self.request.user)

	def post(self, request, *args, **kwargs):
		serializer = self.get_serializer(data={'subject':request.data['subject']})
		serializer.is_valid(raise_exception=True)
		self.perform_create(serializer)
		headers = self.get_success_headers(serializer.data)
		Message.objects.create(sender=request.user, ticket=get_object_or_404(Ticket, pk=serializer.data['id']), body=request.data['body'])
		return response.Response({"result":serializer.data}, status=status.HTTP_201_CREATED, headers=headers)



# async def send_gmail(sub, body, dist):
# 	send_mail(
# 		sub,
# 		body,
# 		'arta1382@gmail.com',
# 		dist, 
# 		fail_silently=False)